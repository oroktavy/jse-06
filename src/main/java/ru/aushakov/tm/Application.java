package ru.aushakov.tm;

import ru.aushakov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                showAppInfo();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showCommandWarning();
        }
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAppInfo() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ANDREY USHAKOV");
        System.out.println("E-MAIL: oroktavy@gmail.com");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.CMD_ABOUT + " - Show general application info");
        System.out.println(TerminalConst.CMD_VERSION + " - Show application version");
        System.out.println(TerminalConst.CMD_HELP + " - Show possible arguments");
        System.out.println(TerminalConst.CMD_EXIT + " - Close application");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showCommandWarning() {
        System.out.println("The command entered is not supported!");
    }

}
